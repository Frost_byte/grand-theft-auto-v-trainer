// Import statements
using GTA;
using System;
using System.Windows.Forms;
using GTA.Native;

public class ScriptTutorial : Script
{
    // Global variables
    bool powerful_winds = false;
    bool unrealistic_physics = false;
    bool fire = false;
    GTA.Ped player;
    GTA.Math.Vector3 pedestrian_force, vehicle_force, force;
   
    public ScriptTutorial() // Constructor
    {
        Tick += OnTick; // One "tick" represents one in-game frame?
        KeyUp += OnKeyUp; // press button once, then let go for effects

        Interval = 10; // Run functions in OnTick function once every second (i.e. continuously)

        // Initialize
        player = Game.Player.Character;
    }

    // Call functions that are to be run continuously in the background 
    void OnTick(object sender, EventArgs e)
    {
        // THINGS THAT SHOULD ALWAYS BE RUNNING, START

        GTA.Native.Function.Call(GTA.Native.Hash._SET_PLAYER_INVINCIBLE_RAGDOLL_ENABLED, true);
        player.ClearBloodDamage();

        // Set weapon strength
        GTA.Native.Function.Call(GTA.Native.Hash.SET_PLAYER_WEAPON_DAMAGE_MODIFIER, player, 1000000000);
        GTA.Native.Function.Call(GTA.Native.Hash.SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER, player, 1000000000);

        foreach (Ped target in GTA.World.GetAllPeds()) // Always disarm pedestrians
        {
            if (target != player)
            {
                GTA.Native.Function.Call(GTA.Native.Hash.REMOVE_ALL_PED_WEAPONS, target); // Disarm all pedestrians 
            }
        }

        // THINGS THAT SHOULD ALWAYS BE RUNNING, END

        // CONDITIONALS START

        if (unrealistic_physics)
        {
            pedestrian_force = GTA.Math.Vector3.Multiply(player.ForwardVector, 1000000000); // Set up force and multiply it by a scalar 
            unrealistic_death_physics_pedestrians(pedestrian_force); // Call Flying pedestrians method

            vehicle_force = GTA.Math.Vector3.Multiply(player.ForwardVector, 9999999999999999999); // Set up force and multiply it by a scalar 
            unrealistic_vehicle_physics(vehicle_force); // Call Flying vehicles method 
        }

        else if (unrealistic_physics == false) // Resets in-game physics
        {
            GTA.Native.Function.Call(GTA.Native.Hash.SET_PLAYER_WEAPON_DAMAGE_MODIFIER, player, 1);
            GTA.Native.Function.Call(GTA.Native.Hash.SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER, player, 1); // Are these default values?

            pedestrian_force.Normalize(); 
            unrealistic_death_physics_pedestrians(pedestrian_force);
            unrealistic_vehicle_physics(pedestrian_force); 
        } 

        if (powerful_winds)
        {
            los_santos_in_chaos();
        }

        else if (powerful_winds == false)
        {
            reset_world();
        }

        

       

        // CONDITIONALS END 

    }

    void reset_world()
    {
        foreach (Ped target in GTA.World.GetAllPeds())
        {
            if (GTA.Native.Function.Call<bool>(GTA.Native.Hash.DOES_ENTITY_EXIST, target) && target != player)
            {
                target.ApplyForce(pedestrian_force * 0); 
                break;
            }
        }
    }
    
    void los_santos_in_chaos()
    {
        foreach (Ped target in GTA.World.GetAllPeds()) 
        {
            if (GTA.Native.Function.Call<bool>(GTA.Native.Hash.DOES_ENTITY_EXIST, target) && target != player) 
            {
                    UI.Notify("Running");
                    target.ApplyForce(pedestrian_force); // Send pedestrian flying away 
                    GTA.Native.Function.Call(GTA.Native.Hash.CLEAR_ENTITY_LAST_DAMAGE_ENTITY, target); // Remove pedestrian from world
                    break; 
            }
        }
    }

    // Sends pedestrians flying when you damage them 
    void unrealistic_death_physics_pedestrians(GTA.Math.Vector3 pedestrian_force)
    {
        foreach (Ped target in GTA.World.GetAllPeds()) // Load all peds in game world
        {
            
            if (GTA.Native.Function.Call<bool>(GTA.Native.Hash.HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY, target, player, true)) // Has pedestrian been injured by player?
            {
                if (GTA.Native.Function.Call<bool>(GTA.Native.Hash.HAS_PED_BEEN_DAMAGED_BY_WEAPON, target, 0, 2)) // Pedestrian has been killed by any weapon
                {
                        target.ApplyForce(pedestrian_force); // Send pedestrian flying away
                        GTA.Native.Function.Call(GTA.Native.Hash.CLEAR_ENTITY_LAST_DAMAGE_ENTITY, target); // Remove pedestrian from world
                        break;
                }

            }

        } 
    }

    // Send vehicles flying when you damage them
    void unrealistic_vehicle_physics(GTA.Math.Vector3 vehicle_force)
    {
        foreach (Vehicle car in GTA.World.GetAllVehicles())
        {
            if (GTA.Native.Function.Call<bool>(GTA.Native.Hash.HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY, car, player, true))
            {
                if (GTA.Native.Function.Call<bool>(GTA.Native.Hash.HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON, car, 0, 2))
                {
                        car.ApplyForce(vehicle_force); // Send vehicle flying away
                        GTA.Native.Function.Call(GTA.Native.Hash.CLEAR_ENTITY_LAST_DAMAGE_ENTITY, car); // Remove vehicle from world
                        break;
                }
            }

        }
    } 

    // Do stuff that requires a button to be pressed only once, then let go of
    void OnKeyUp(object sender, KeyEventArgs e)
    {
        // UNREALISTIC PHYSICS BEGIN

        if (e.KeyCode == Keys.NumPad1 && unrealistic_physics == false)
        {
            unrealistic_physics = true;
            UI.ShowSubtitle("Unrealistic Physics ON", 5000); 

        }

        else if (e.KeyCode == Keys.NumPad1 && unrealistic_physics)
        {
            unrealistic_physics = false;
            UI.ShowSubtitle("Unrealistic Physics OFF", 5000);
        }

        // UNREALISTIC PHYSICS END

        // POWERFUL WINDS START

        if (e.KeyCode == Keys.NumPad4 && powerful_winds == false)
        {
            powerful_winds = true;
            UI.ShowSubtitle("Powerful Winds ON", 5000);
        }

        else if (e.KeyCode == Keys.NumPad4 && powerful_winds)
        {
            powerful_winds = false;
            UI.ShowSubtitle("Powerful Winds OFF", 5000);
        }

        // POWERFUL WINDS END

        // FIRE START

        if (e.KeyCode == Keys.NumPad7 && fire == false)
        {
            fire = true;
            UI.ShowSubtitle("Fire ON", 5000);
        }

        else if (e.KeyCode == Keys.NumPad7 && fire)
        {
            fire = false;
            UI.ShowSubtitle("Fire OFF", 5000);
        } 

        // FIRE END

    }


}